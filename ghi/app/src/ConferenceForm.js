import React, { useEffect, useState } from 'react';

export default function ConferenceForm() {

    const [locations, setLocations] = useState([]);
    const [name, setName] = useState("");
    const [starts, setStarts] = useState("");
    const [ends, setEnds] = useState("");
    const [description, setDescription] = useState("");
    const [maxPresentations, setMaxPresentations] = useState("");
    const [maxAttendees, setMaxAttendees] = useState("");
    const [location, setLocation] = useState("");

    const handleUpdate = function(e, stateFunction) {
        const value = e.target.value;
        stateFunction(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};

        data.name = name;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.starts = starts;
        data.ends = ends;
        data.location = location;

        console.log(data);

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log (newConference);

            setName("");
            setStarts("");
            setEnds("");
            setDescription("");
            setMaxPresentations("");
            setMaxAttendees("");
            setLocation("");
        }
    }

    const fetchData = async() => {
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
            console.log(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form id="create-conference-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                onChange={(e) => handleUpdate(e, setName)}
                                value={name}
                                placeholder="Name"
                                required=""
                                type="text"
                                name="name"
                                id="name"
                                className="form-control"
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={(e) => handleUpdate(e, setStarts)}
                                value={starts}
                                placeholder="Starts"
                                required=""
                                type="date"
                                name="starts"
                                id="starts"
                                className="form-control"
                            />
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={(e) => handleUpdate(e, setEnds)}
                                value={ends}
                                placeholder="Ends"
                                required=""
                                type="date"
                                name="ends"
                                id="ends"
                                className="form-control"
                            />
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">
                                Description
                            </label>
                            <textarea
                                onChange={(e) => handleUpdate(e, setDescription)}
                                value={description}
                                name="description"
                                id="description"
                                className="form-control"
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={(e) => handleUpdate(e, setMaxPresentations)}
                                value={maxPresentations}
                                placeholder="Max presentations"
                                required=""
                                type="number"
                                name="max_presentations"
                                id="max_presentations"
                                className="form-control"
                            />
                            <label htmlFor="max_presentations">Max presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={(e) => handleUpdate(e, setMaxAttendees)}
                                value={maxAttendees}
                                placeholder="Max attendees"
                                required=""
                                type="number"
                                name="max_attendees"
                                id="max_attendees"
                                className="form-control"
                            />
                            <label htmlFor="max_attendees">Max attendees</label>
                        </div>
                        <div className="mb-3">
                            <select
                                onChange={(e) => handleUpdate(e, setLocation)}
                                value={location}
                                required=""
                                name="location"
                                id="location"
                                className="form-select"
                            >
                                <option value="">
                                Choose a location
                                </option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
