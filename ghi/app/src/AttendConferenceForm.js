import React, { useEffect, useState } from 'react';

export default function AttendConferenceForm() {
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [signedUp, setSignedUp] = useState(false);

    const handleUpdate = function(e, stateFunction) {
        const value = e.target.value;
        stateFunction(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};

        data.conference = conference;
        data.name = name;
        data.email = email;

        console.log(data);

        const attendeeUrl = "http://localhost:8001/api/attendees/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();
            console.log(newAttendee);

            setConference("");
            setName("");
            setEmail("");
            setSignedUp(true);
        }
    }

    let spinnerClass = "d-flex justify-content-center mb-3";
    let dropdownClasses = "form-select d-none";
    if (conferences.length > 0) {
        spinnerClass = "d-flex justify-content-center mb-3 d-none";
        dropdownClasses = "form-select";
    }

    const fetchData = async() => {
        const url = "http://localhost:8000/api/conferences/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
            console.log(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    let formClass = "";
    let alertClass = "alert alert-success d-none mb-0";
    // if (signedUp === true) {
    //     formClass = "d-none";
    //     alertClass = "d-flex justify-content-center mb-3";
    // }

    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img
                        width={300}
                        className="bg-white rounded shadow d-block mx-auto mb-4"
                        src="./logo.svg"
                    />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form id="create-attendee-form" onSubmit={handleSubmit} className={formClass}>
                                <h1 className="card-title">It's Conference Time!</h1>
                                <p className="mb-3">
                                Please choose which conference you'd like to attend.
                                </p>
                                <div
                                className={spinnerClass}
                                id="loading-conference-spinner"
                                >
                                    <div className="spinner-grow text-secondary" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <select
                                        onChange={(e) => handleUpdate(e, setConference)}
                                        value={conference}
                                        name="conference"
                                        id="conference"
                                        className={dropdownClasses}
                                        required=""
                                    >
                                        <option value="">Choose a conference</option>
                                        {conferences.map(conference => {
                                            return(
                                                <option key={conference.href} value={conference.href}>
                                                    {conference.name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <p className="mb-3">Now, tell us about yourself.</p>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input
                                                onChange={(e) => handleUpdate(e, setName)}
                                                value={name}
                                                required=""
                                                placeholder="Your full name"
                                                type="text"
                                                id="name"
                                                name="name"
                                                className="form-control"
                                            />
                                            <label htmlFor="name">Your full name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input
                                                onChange={(e) => handleUpdate(e, setEmail)}
                                                value={email}
                                                required=""
                                                placeholder="Your email address"
                                                type="email"
                                                id="email"
                                                name="email"
                                                className="form-control"
                                            />
                                            <label htmlFor="email">Your email address</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">I'm going!</button>
                            </form>
                            <div className={alertClass} id="success-message">
                                Congratulations! You're all signed up!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
