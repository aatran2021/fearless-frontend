window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error ("Response not ok");
        } else {
            const data = await response.json();

            const selectTag = document.getElementById('conference');
            for (let conference of data.conferences) {
                const option = document.createElement('option');
                option.value = conference.href;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }

            console.log(selectTag);
            selectTag.classList.remove("d-none");
            const loader = document.getElementById("loading-conference-spinner");
            loader.classList.add("d-none");
        }

        const formTag = document.getElementById("create-attendee-form");
        formTag.addEventListener("submit", async (e) => {
            e.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const attendeeUrl = "http://localhost:8001/api/attendees/";
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    "Content-Type": "application/json",
                },
            };

            const response = await fetch(attendeeUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newAttendee = await response.json();

                const successTag = document.getElementById("success-message");
                successTag.classList.remove("d-none");
                formTag.classList.add("d-none");
            }
        })
    } catch (e) {
        console.error("error", e);
    }
});
