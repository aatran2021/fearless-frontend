// function deletePlaceholders() {
//     let placeholders = document.querySelectorAll('.placeholder');
//     placeholders.forEach(placeholder => {
//         placeholder.classList.remove('placeholder');
//     })
// }

// function createCard(name, description, pictureUrl, dateStartReturn, dateEndReturn, location) {
//     return `
//         <div class="col">
//             <div class="card mb-3 shadow placeholder-glow">
//                 <img src="${pictureUrl}" class="card-img-top placeholder">
//                 <div class="card-body">
//                     <h5 class="card-title placeholder">${name}</h5>
//                     <p class="card-subtitle mb-2 text-secondary placeholder">${location}</p>
//                     <p class="card-text placeholder">${description}</p>
//                 </div>
//                 <div class="card-footer placeholder">
//                     ${dateStartReturn} - ${dateEndReturn}
//                 </div>
//             </div>
//         </div>
//     `;
//   }

function createCard(name, description, pictureUrl, dateStartReturn, dateEndReturn, location) {
    return `
        <div class="col">
            <div class="card mb-3 shadow">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <p class="card-subtitle mb-2 text-secondary">${location}</p>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    ${dateStartReturn} - ${dateEndReturn}
                </div>
            </div>
        </div>
    `;
  }

// Create placeholder function (similar to createCard)
function createPlaceholder() {
    return `
    <div class="col del-placeholder">
        <div class="card mb-3 shadow" aria-hidden="true">
            <div class="card-body">
                <h5 class="card-title placeholder-glow">
                    <span class="placeholder col-6"></span>
                </h5>
                <p class="card-text placeholder-glow">
                    <span class="placeholder col-7"></span>
                    <span class="placeholder col-4"></span>
                    <span class="placeholder col-4"></span>
                    <span class="placeholder col-6"></span>
                    <span class="placeholder col-8"></span>
                </p>
                <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
            </div>
        </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error ('Response not ok | Issue during fetching and processing of data');
        } else {
            const data = await response.json();
            for (let i = 0; i < data.conferences.length; i++) {
                let placeholder = createPlaceholder();
                const row = document.querySelector(".row");
                row.innerHTML += placeholder;
            }

            // Loops through all the conferences
            for (let conference of data.conferences) {
                // Grabs conference details
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);



                // If promises resolves grab details from response object
                if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  // Create bootstrap card with data from response object
                  const title = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;

                  const dateStart = new Date(details.conference.starts);
                  const dateStartDay = dateStart.getDate();
                  const dateStartMonth = dateStart.getMonth();
                  const dateStartYear = dateStart.getFullYear();
                  const dateStartReturn = `${dateStartMonth}/${dateStartDay}/${dateStartYear}`


                  const dateEnd = new Date(details.conference.ends);
                  const dateEndDay = dateEnd.getDate();
                  const dateEndMonth = dateEnd.getMonth();
                  const dateEndYear = dateEnd.getFullYear();
                  const dateEndReturn = `${dateEndMonth}/${dateEndDay}/${dateEndYear}`

                  const location = details.conference.location.name;

                  const html = createCard(title, description, pictureUrl, dateStartReturn, dateEndReturn, location);
                  // Add card into HTML column tag
                  const row = document.querySelector(".row");
                  const placeholder = document.querySelector(".del-placeholder");
                  row.removeChild(placeholder);
                  row.innerHTML = html + row.innerHTML;
                //   setTimeout(() => {
                //     deletePlaceholders();
                //   }, 500);
                }
            }

        }
    } catch (e) {
        console.error('error', e);
        const row = document.querySelector(".container");
        row.innerHTML += `
            <div class="alert alert-primary" role="alert">${e}</div>
        `;
    }
});
