window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/conferences/";

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error ("Response not ok");
        } else {
            const data = await response.json();
            console.log(data.conferences)
            const dropdown = document.querySelector("#conference");
            for (let conference of data.conferences) {
                const option = document.createElement("option");

                const href = conference.href
                const href_array = href.split("/")
                const id = href_array[3];

                option.value = id;
                option.innerHTML = conference.name;
                dropdown.appendChild(option);
            }
        }

        const formTag = document.getElementById("create-presentation-form");
        formTag.addEventListener("submit", async (e) => {
            e.preventDefault();
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData));

            const dropdown = document.querySelector("#conference");
            const dropdownValue = dropdown.options[dropdown.selectedIndex].value

            const presentationUrl = `http://localhost:8000/api/conferences/${dropdownValue}/presentations/`;
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    "Content-Type": "application/json",
                },
            };

            const response = await fetch(presentationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newPresentation = await response.json();
            }

        })

    } catch(e) {
        console.error("error", e);
    }
})
